package tonespy.imagecapture;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.media.MediaRecorder;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener, SurfaceHolder.Callback {

    private Button startRecord;
    private SurfaceView videoCapture;
    private MediaRecorder mMediaRecoreder;
    private Camera mCamera;
    private SurfaceHolder mHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Activity Orientation Mode
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        startRecord = (Button) findViewById(R.id.startRecord);
        startRecord.setOnTouchListener(this);
        videoCapture = (SurfaceView) findViewById(R.id.videoCapture);
        mHolder = videoCapture.getHolder();
        mHolder.addCallback(this);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (checkPermissions()) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    if (mMediaRecoreder == null) {
                        try {
                            initVideoRecorder(mHolder.getSurface());
                            if(mMediaRecoreder != null) {
                                mMediaRecoreder.start();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        mMediaRecoreder.start();
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    //mMediaRecoreder.stop();
                    destroy();
                    break;
            }
            return true;
        }
        return false;
    }

    private boolean checkPermissions() {
        if (hasPermissionInManifest("android.permission.RECORD_AUDIO") && hasPermissionInManifest("android.permission.CAMERA") && hasPermissionInManifest("android.permission.READ_EXTERNAL_STORAGE")) {
            return true;
        }
        return false;
    }

    private void initVideoRecorder(Surface surface) throws IOException {

        if(mCamera == null) {
            mCamera = Camera.open();
            mCamera.unlock();
        }

        if (mMediaRecoreder == null) {
            mMediaRecoreder = new MediaRecorder();
        }
        mMediaRecoreder.setPreviewDisplay(surface);
        mMediaRecoreder.setCamera(mCamera);

        mMediaRecoreder.setVideoSource(MediaRecorder.VideoSource.DEFAULT);
        mMediaRecoreder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mMediaRecoreder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
        mMediaRecoreder.setVideoEncodingBitRate(512 * 1000);
        mMediaRecoreder.setVideoFrameRate(30);
        mMediaRecoreder.setVideoSize(640, 480);
        mMediaRecoreder.setOutputFile(getOutputMediaFile().getPath());

        try {
            mMediaRecoreder.prepare();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            initVideoRecorder(mHolder.getSurface());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        //
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        destroy();
    }

    private void destroy() {
        if (mMediaRecoreder != null) {
            mMediaRecoreder.reset();
            mMediaRecoreder.release();
            mCamera.release();

            mMediaRecoreder = null;
            mCamera = null;
        }
    }

    private static File getOutputMediaFile() {

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES), "TestVideos");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("VideoTest", "Failed to create directory");
                return null;
            }
        }

        //Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator + "VID_" + timeStamp + ".mp4");
    }

    public boolean hasPermissionInManifest (String permissionName) {
        final String packageName = getPackageName();
        try {
            final PackageInfo packageInfo = getPackageManager()
                    .getPackageInfo(packageName, PackageManager.GET_PERMISSIONS);
            final String[] declaredPermisisons = packageInfo.requestedPermissions;
            if (declaredPermisisons != null && declaredPermisisons.length > 0) {
                for (String p : declaredPermisisons) {
                    if (p.equals(permissionName)) {
                        return true;
                    }
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }
}
